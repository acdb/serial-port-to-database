import xlwt
from decimal import Decimal


def initExcel():
    book = xlwt.Workbook(encoding='utf-8', style_compression=0)
    sheet = book.add_sheet('data', cell_overwrite_ok=True)
    sheet.write(0, 0, "data")
    return book, sheet


count = 0


def analysis(data, book):
    global count
    try:
        buf = data.split(':')[2].split(',')[0].replace('\n', '')
        buf = Decimal(buf)
        count = count + 1
        book.write(count, 0, buf)
    except:
        print("error\n")
    print(count)
    # print(buf)
