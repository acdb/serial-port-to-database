import sys

from PyQt5 import sip
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QHBoxLayout, QVBoxLayout, QPushButton, QGridLayout, QRadioButton, QLabel, QLineEdit


class DatabaseUi(QDialog):
    buf = {"SQLite": None, "MySQL": None}
    wbuf = 0
    def __init__(self):
        super().__init__()
        self.main_vbox = QVBoxLayout()  # 主纵向布局
        self.top_hbox = QHBoxLayout()  # 顶部横向布局
        self.bottom_hbox = QHBoxLayout()  # 底部横向布局
        self.left_vbox = QVBoxLayout()  # 中层左侧纵向布局
        self.right_top_Gbox = QGridLayout()  # 中层右侧网格布局
        self.addr_label = QLabel("服务器地址")
        self.username_label = QLabel("用户名")
        self.password_label = QLabel("密码")
        self.saveButton = QPushButton("保存")
        self.exitButton = QPushButton("退出")

        self.addr_edit = QLineEdit()
        self.username_edit = QLineEdit()
        self.password_edit = QLineEdit()

        self.initDatabaseUi()

    def initDatabaseUi(self):
        # 显示任务栏图标
        if sys.platform == "win32":
            import ctypes
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID("6666")
        # 设置窗口的标题
        self.setWindowTitle('数据库选择')
        self.resize(300, 150)
        # 设置窗口ICON
        self.setWindowIcon(QIcon("image/星球.png"))
        self.bottom_hbox.addWidget(self.saveButton)
        self.bottom_hbox.addWidget(self.exitButton)
        for i in self.buf:
            print(i)
            btn = QRadioButton(i)
            self.buf[i] = btn  # 向字典中添加按钮缓存
            self.left_vbox.addWidget(btn)
            btn.toggled.connect(self.__buttonMsg)

        self.top_hbox.addLayout(self.left_vbox)  # 添加左侧纵向布局
        # self.top_hbox.addLayout(self.right_top_Gbox)  # 添加右侧网格布局

        self.main_vbox.addLayout(self.top_hbox)  # 添加顶部横向布局
        self.main_vbox.addLayout(self.bottom_hbox)  # 添加底部横向布局
        self.setLayout(self.main_vbox)

    def __buttonMsg(self):
        for i in self.buf:  # 遍历字典查找按钮
            button = self.buf.get(i)
            if self.sender() == button:
                if button.isChecked():
                    print(i, "选中")
                    if i == "MySQL":
                        self.addr_label = QLabel("服务器地址")
                        self.username_label = QLabel("用户名")
                        self.password_label = QLabel("密码")
                        self.addr_edit = QLineEdit()
                        self.username_edit = QLineEdit()
                        self.password_edit = QLineEdit()
                        self.right_top_Gbox.addWidget(self.addr_label, 0, 0)
                        self.right_top_Gbox.addWidget(self.addr_edit, 0, 1)
                        self.right_top_Gbox.addWidget(self.username_label, 1, 0)
                        self.right_top_Gbox.addWidget(self.username_edit, 1, 1)
                        self.right_top_Gbox.addWidget(self.password_label, 2, 0)
                        self.right_top_Gbox.addWidget(self.password_edit, 2, 1)

                        self.top_hbox.addLayout(self.right_top_Gbox)
                        self.update()
                    if i == "SQLite":
                        print("删除控件")
                        # self.wbuf = self.right_top_Gbox.count()
                        # if self.wbuf == 0:
                        #     return
                        print(self.right_top_Gbox.count())
                        while self.right_top_Gbox.count():
                            wbuf = self.right_top_Gbox.itemAt(0).widget()
                            self.right_top_Gbox.removeWidget(wbuf)
                            sip.delete(wbuf)
                        self.update()
