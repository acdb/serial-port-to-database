from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QPushButton, \
    QHBoxLayout, QVBoxLayout, QDialog, QTextEdit, QRadioButton


class AnalysisUi(QDialog):
    buf = {"Json": "None", "自定义": None}

    def __init__(self):
        super().__init__()
        self.main_hbox = QHBoxLayout()
        self.left_vbox = QVBoxLayout()
        self.right_text = QTextEdit()
        self.saveButton = QPushButton("保存")
        self.exitButton = QPushButton("退出")
        self.initAnalysisUi()

    def initAnalysisUi(self):
        # 设置窗口的标题
        self.setWindowTitle('解析格式选择')
        # 设置窗口ICON
        self.setWindowIcon(QIcon("image/星球.png"))
        self.main_hbox.addLayout(self.left_vbox)
        self.main_hbox.addWidget(self.right_text)
        for i in self.buf:
            print(i)
            btn = QRadioButton(i)
            self.buf[i] = btn  # 向字典中添加按钮缓存
            self.left_vbox.addWidget(btn)
            btn.toggled.connect(self.__buttonMsg)
        self.left_vbox.addWidget(self.saveButton)
        self.left_vbox.addWidget(self.exitButton)
        self.setLayout(self.main_hbox)

        self.saveButton.clicked.connect(self.__buttonMsg)  # 保存按钮信号
        self.exitButton.clicked.connect(self.__buttonMsg)  # 退出按钮信号

    def __buttonMsg(self):
        if self.sender() == self.saveButton:
            print("保存按钮")
            return
        if self.sender() == self.exitButton:
            print("退出按钮")
            self.close()  # 关闭窗口
            return
        for i in self.buf:  # 遍历字典查找按钮
            button = self.buf.get(i)
            if self.sender() == button:
                if button.isChecked():
                    print(i, "选中")
